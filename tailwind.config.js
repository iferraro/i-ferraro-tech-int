module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        theme: ["Alegreya Sans", "sans-serif"],
      },
      colors: {
        "theme-light": "#EDEACE",
        "theme-dark": "#00201C",
        "theme-3": "#0E6359",
      },
    },
  },
  plugins: [require("tailwind-scrollbar")],
};
