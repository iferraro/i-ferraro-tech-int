import { useState } from "react";

import ChoiceList from "./components/ChoiceList";
import ChoiceListLogo from "./components/ChoiceListLogo";
import { states } from "./data/stateOptions";

const App = () => {
  const [listAValue, setListAValue] = useState<string>("");
  const [listBValue, setListBValue] = useState<string[]>([]);
  const [listCValue, setListCValue] = useState<string>("");
  const [listDValue, setListDValue] = useState<string[]>([]);

  return (
    <div className="flex flex-col justify-center items-center min-h-screen p-4 font-theme bg-theme-light">
      <h1 className="flex flex-row items-center m-4 text-6xl text-theme-dark font-bold">
        <ChoiceListLogo />
        <span>hoiceLists</span>
      </h1>
      <ChoiceList
        label="Single option"
        selected={listAValue}
        options={[
          { label: "Option A", value: "a" },
          { label: "Option B", value: "b" },
          { label: "Option C", value: "c" },
          { label: "Option D", value: "d" },
          { label: "Option E", value: "e" },
        ]}
        onSelect={setListAValue}
      />

      <ChoiceList
        label="Multiple options"
        selected={listBValue}
        options={[
          { label: "Option A", value: "a" },
          { label: "Option B", value: "b" },
          { label: "Option C", value: "c" },
          { label: "Option D", value: "d" },
          { label: "Option E", value: "e" },
        ]}
        allowMultiple
        onSelect={setListBValue}
      />

      <ChoiceList
        label="Single option w/ search"
        selected={listCValue}
        options={states}
        allowSearch
        onSelect={setListCValue}
      />

      <ChoiceList
        label="Multiple options w/ search"
        selected={listDValue}
        options={states}
        allowSearch
        allowMultiple
        onSelect={setListDValue}
      />
    </div>
  );
};

export default App;
