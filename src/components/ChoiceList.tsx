import React, { useState, SetStateAction, useMemo } from "react";
import { getSearchResults } from "../utils/getSearchResults";

interface ChoiceListProps {
  label: string;
  selected: string | string[];
  options: Array<{ label: string; value: string }>;
  onSelect:
    | React.Dispatch<React.SetStateAction<string>>
    | React.Dispatch<React.SetStateAction<string[]>>;
  allowMultiple?: boolean;
  allowSearch?: boolean;
}

const ChoiceList = ({
  label,
  selected,
  options,
  onSelect,
  allowMultiple,
  allowSearch,
}: ChoiceListProps) => {
  const [query, setQuery] = useState<string>("");
  const [revealed, setRevealed] = useState<boolean>(false);

  const isString = (item: any) => {
    return typeof item === "string";
  };

  const isArray = (item: any) => {
    return item instanceof Array;
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!allowMultiple && isString(selected)) {
      (onSelect as React.Dispatch<SetStateAction<string>>)("");
    }
    setRevealed(true);
    setQuery(e.target.value);
  };

  const matches: string[] = useMemo(() => {
    return getSearchResults(query, options);
  }, [query]);

  const handleReveal = () => {
    setRevealed((revealed) => !revealed);
    setQuery("");
  };

  const handleClick = (value: string) => {
    if (isArray(selected)) {
      (onSelect as React.Dispatch<SetStateAction<string[]>>)(() => {
        const newState = [...selected];
        if (newState.includes(value)) {
          newState.splice(newState.indexOf(value), 1);
          return newState;
        }
        if (!newState.includes(value)) {
          newState.push(value);
          return newState;
        }
        return newState;
      });
    }
    if (isString(selected)) {
      (onSelect as React.Dispatch<SetStateAction<string>>)(value);
    }
    if (!allowMultiple) {
      setRevealed(false);
    }
    return;
  };

  window.addEventListener("click", (e: MouseEvent) => {
    const choiceLists: HTMLCollectionOf<Element> =
      document.getElementsByClassName("choicelist");
    let clicksOutside: number = 0;
    for (let i: number = 0; i < choiceLists.length; i++) {
      if (!choiceLists[i].contains(e.target as Node)) {
        clicksOutside++;
      }
    }
    if (clicksOutside === choiceLists.length) {
      setRevealed(false);
    }
    return;
  });

  const generateDescription = () => {
    const description = !allowSearch
      ? !allowMultiple
        ? "I. Single Option Without Search"
        : "II. Multiple Options Without Search"
      : !allowMultiple
      ? "III. Single Option With Search"
      : "IV. Multiple Options With Search";
    return (
      <h1 className="my-2 text-center text-xl text-theme-dark">
        {description}
      </h1>
    );
  };

  const generateButton = () => {
    const generateButtonText = () => {
      if (isString(selected) && selected !== "") {
        return selected;
      }
      if (isArray(selected)) {
        if (selected.length > 0) {
          if (selected.length > 1) {
            return `${selected[0]} + ${selected.length - 1}`;
          }
          return selected[0];
        }
        return "One or more options";
      }
      return options[0].label;
    };

    return (
      <button
        type="button"
        className="w-full h-12 bg-theme-dark font-medium text-lg text-theme-light rounded-lg hover:bg-theme-3"
        onClick={handleReveal}
      >
        {generateButtonText()}
      </button>
    );
  };

  const generateInput = () => {
    const generatePlaceholder = () => {
      if (typeof selected === "string") {
        if (selected !== "") {
          return selected;
        }
        return "Search for a state";
      }
      if (isArray(selected)) {
        if (selected.length > 0) {
          if (selected.length > 1) {
            return `${selected[0]} + ${selected.length - 1}`;
          }
          return selected[0];
        }
        return "Search for multiple states";
      }
      return "No placeholder";
    };

    const generateValue = () => {
      if (isString(selected) && selected !== "") {
        return selected;
      }
      return query;
    };

    return (
      <input
        type="text"
        title={label}
        placeholder={generatePlaceholder()}
        value={generateValue()}
        onChange={handleChange}
        className="w-full h-12 px-2 text-lg text-black placeholder:text-slate-700 bg-theme-light border-4 border-theme-dark rounded-lg hover:border-theme-3 focus:outline-none"
        onClick={handleReveal}
      />
    );
  };

  const generateAnOption = (
    item: string | { label: string; value: string }
  ) => {
    const itemIsString = typeof item === "string";
    const displayedText = itemIsString ? item : item.label;
    const itemIsInSelected =
      isArray(selected) && selected.includes(displayedText);
    return (
      <div
        key={itemIsString ? item : item.value}
        onClick={() => handleClick(displayedText)}
        className={
          itemIsInSelected
            ? "m-1 p-2 text-theme-light bg-theme-dark rounded"
            : "m-1 p-2 text-theme-dark bg-theme-light rounded hover:text-theme-light hover:bg-theme-3"
        }
      >
        {displayedText}
      </div>
    );
  };

  const generateOptions = () => {
    const dataSet = allowSearch ? matches : options;
    return (
      <div className="absolute left-0 top-[100px] w-full max-h-80 bg-theme-light border-4 border-theme-dark rounded-lg overflow-y-auto scrollbar scrollbar-thumb-theme-dark scrollbar-track-theme-light z-10 hover:border-theme-3 hover:scrollbar-thumb-theme-3">
        {dataSet.length > 0 &&
          dataSet.map((item) => {
            return generateAnOption(item);
          })}
      </div>
    );
  };

  return (
    <div className="choicelist relative w-72 m-4">
      {generateDescription()}
      {!allowSearch && generateButton()}
      {allowSearch && generateInput()}
      {revealed && generateOptions()}
    </div>
  );
};

export default ChoiceList;
