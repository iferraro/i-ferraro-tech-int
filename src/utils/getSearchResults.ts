const getSearchResults = (
  query: string,
  dataSet: Array<{ label: string; value: string }>
) => {
  let matchingData: string[] = [];
  if (query !== "" && query !== null && query !== undefined) {
    dataSet.forEach((item) => {
      if (item.label.toLowerCase().includes(query.toLowerCase())) {
        matchingData.push(item.label);
      }
    });
    if (matchingData.length > 0) {
      return matchingData;
    }
    return [`No results found for "${query}"`];
  }
  if (query === "" || query === null || query === undefined) {
    matchingData = dataSet.map((item) => {
      return item.label;
    });
    return matchingData;
  }
  return ["Nothing found"];
};

export { getSearchResults };
